package com.ady.topquiz.model

class User {
    private var mFirstName = ""

    fun getFirstName(): String? {
        return mFirstName
    }

    fun setFirstName(value: String) {
        mFirstName = value
    }
}
