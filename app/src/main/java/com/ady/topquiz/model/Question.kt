package com.ady.topquiz.model

class Question(
    private var mQuestion: String? = null,
    private var mChoiceList: List<String> = ArrayList(),
    private var mAnswerIndex: Int = 0
) {
    fun getQuestion(): String? {
        return mQuestion
    }

    fun setQuestion(value: String) {
        mQuestion = value
    }

    fun getChoiceList(): List<String> {
        return mChoiceList
    }

    fun setChoiceList(list: List<String>) {
        mChoiceList = list
    }

    fun addChoice(value: String) {
        mChoiceList.plus(value)
    }

    fun getAnswerIndex(): Int {
        return mAnswerIndex
    }

    fun setAnswerIndex(value: Int) {
        mAnswerIndex = value
    }
}
