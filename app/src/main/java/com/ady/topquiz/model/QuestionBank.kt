package com.ady.topquiz.model

import java.util.Collections.shuffle

class QuestionBank(
    questionList: List<Question>
) {
    private var mQuestionList: List<Question> = questionList.shuffled()
    private var mNextQuestionIndex = 0

    fun getQuestion(): Question {
        if (mNextQuestionIndex > mQuestionList.size) {
            mNextQuestionIndex = 0
        }

        return mQuestionList[mNextQuestionIndex++]
    }
}
