package com.ady.topquiz

import android.R.attr
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.ady.topquiz.model.User


class MainActivity : AppCompatActivity() {
    private val GAME_ACTIVITY_REQUEST_CODE = 42
    private val FIRST_NAME_KEY = "firstName"
    private val SCORE_KEY = "score"
    private lateinit var mWelcomeText: TextView
    private lateinit var mNameInput: EditText
    private lateinit var mPlayButton: Button
    private lateinit var mUser: User
    private lateinit var mPreferences: SharedPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        mUser = User()
        mPreferences = getPreferences(Context.MODE_PRIVATE)

        mWelcomeText = findViewById(R.id.activity_main_welcome_txt)
        mNameInput = findViewById(R.id.activity_main_name_input)
        mPlayButton = findViewById(R.id.activity_main_start_button)

        mPlayButton.isEnabled = false

        greetUser()

        mNameInput.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(
                s: CharSequence,
                start: Int,
                count: Int,
                after: Int
            ) {
            }

            override fun onTextChanged(
                s: CharSequence,
                start: Int,
                before: Int,
                count: Int
            ) {
                mPlayButton.isEnabled = s.toString().length >= 3
            }

            override fun afterTextChanged(s: Editable) {}
        })

        mPlayButton.setOnClickListener {
            mUser.setFirstName(mNameInput.text.toString())
            mPreferences.edit().putString(FIRST_NAME_KEY, mUser.getFirstName()).apply()

            val gameActivity = Intent(this@MainActivity, GameActivity::class.java)
            startActivityForResult(gameActivity, GAME_ACTIVITY_REQUEST_CODE)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == GAME_ACTIVITY_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            val score: Int = data!!.getIntExtra(GameActivity().BUNDLE_EXTRA_SCORE, 0)
            mPreferences.edit().putInt(SCORE_KEY, score).apply()

            greetUser()
        }
    }

    private fun greetUser() {
        val firstName: String? = mPreferences.getString(FIRST_NAME_KEY, null)

        if (firstName !== null) {
            val score: Int? = mPreferences.getInt(SCORE_KEY, 0)

            mWelcomeText.text = getString(R.string.welcome_back, firstName, score)
            mNameInput.setText(firstName)
            mNameInput.setSelection(firstName.length);

            mPlayButton.isEnabled = true
        }
    }
}
