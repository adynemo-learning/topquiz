package com.ady.topquiz

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.view.MotionEvent
import android.view.View
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import android.widget.Toast.LENGTH_SHORT
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.ady.topquiz.model.Question
import com.ady.topquiz.model.QuestionBank


class GameActivity : AppCompatActivity(), View.OnClickListener {
    val BUNDLE_EXTRA_SCORE = "BUNDLE_EXTRA_SCORE"
    private lateinit var mQuestionText: TextView
    private lateinit var mAnswer1Button: Button
    private lateinit var mAnswer2Button: Button
    private lateinit var mAnswer3Button: Button
    private lateinit var mAnswer4Button: Button
    private lateinit var mAnswerButtons: List<Button>
    private lateinit var mQuestionBank: QuestionBank
    private lateinit var mCurrentQuestion: Question
    private var mNumberOfQuestions: Int = 4
    private var mScore: Int = 0
    private var mEnableTouchEvents: Boolean = true
    val BUNDLE_STATE_SCORE = "currentScore"
    val BUNDLE_STATE_QUESTION = "currentQuestion"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_game)

        mQuestionBank = generateQuestionBank()

        if (savedInstanceState != null) {
            mScore = savedInstanceState.getInt(BUNDLE_STATE_SCORE);
            mNumberOfQuestions = savedInstanceState.getInt(BUNDLE_STATE_QUESTION);
        }

        mQuestionText = findViewById(R.id.activity_game_question_text)
        mAnswer1Button = findViewById(R.id.activity_game_answer1_btn)
        mAnswer2Button = findViewById(R.id.activity_game_answer2_btn)
        mAnswer3Button = findViewById(R.id.activity_game_answer3_btn)
        mAnswer4Button = findViewById(R.id.activity_game_answer4_btn)
        mAnswerButtons = listOf(mAnswer1Button, mAnswer2Button, mAnswer3Button, mAnswer4Button)

        for ((i, answerButton) in mAnswerButtons.withIndex()) {
            // Use the same listener for the four buttons.
            // The tag value will be used to distinguish the button triggered
            answerButton.setOnClickListener(this);
            // Use the tag property to 'name' the buttons
            answerButton.tag = i;
        }

        mCurrentQuestion = mQuestionBank.getQuestion()
        displayQuestion(mCurrentQuestion)
    }

    override fun onClick(v: View) {
        mEnableTouchEvents = false
        val responseIndex = v.tag as Int

        if (responseIndex != mCurrentQuestion.getAnswerIndex()) {
            // Wrong answer
            Toast.makeText(this, "Wrong answer! The right is: " + mCurrentQuestion.getChoiceList()[mCurrentQuestion.getAnswerIndex()], LENGTH_SHORT).show()
        } else {
            // Right answer
            ++mScore
            Toast.makeText(this, "Correct!", LENGTH_SHORT).show()
        }

        Handler().postDelayed(Runnable {
            if (--mNumberOfQuestions == 0) {
                // End of game
                val builder: AlertDialog.Builder = AlertDialog.Builder(this)

                builder.setTitle("Well done!")
                    .setMessage("Your score is $mScore")
                    .setPositiveButton(
                        "OK"
                    ) { dialog, which -> // End the activity
                        val intent = Intent()
                        intent.putExtra(BUNDLE_EXTRA_SCORE, mScore)
                        setResult(Activity.RESULT_OK, intent)
                        finish()
                    }
                    .setCancelable(false)
                    .create()
                    .show()
            } else {
                mCurrentQuestion = mQuestionBank.getQuestion()
                displayQuestion(mCurrentQuestion)
                mEnableTouchEvents = true
            }
        }, 2000) // LENGTH_SHORT is usually 2 second long
    }

    private fun displayQuestion(question: Question) {
        // Set the text for the question text view and the four buttons
        mQuestionText.text = question.getQuestion()

        for ((i, answer) in question.getChoiceList().withIndex()) {
            mAnswerButtons[i].text = answer
        }
    }

    override fun dispatchTouchEvent(ev: MotionEvent?): Boolean {
        return mEnableTouchEvents && super.dispatchTouchEvent(ev)
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putInt(BUNDLE_STATE_SCORE, mScore)
        outState.putInt(BUNDLE_STATE_QUESTION, mNumberOfQuestions)

        super.onSaveInstanceState(outState)
    }

    private fun generateQuestionBank(): QuestionBank {
        val question1 = Question(
            "What is the name of the current french president?",
            listOf(
                "François Hollande",
                "Emmanuel Macron",
                "Jacques Chirac",
                "François Mitterand"
            ),
            1
        )

        val question2 = Question(
            "How many countries are there in the European Union?",
            listOf("15", "24", "28", "32"),
            2
        )

        val question3 = Question(
            "Who is the creator of the Android operating system?",
            listOf("Andy Rubin", "Steve Wozniak", "Jake Wharton", "Paul Smith"),
            0
        )

        val question4 = Question(
            "When did the first man land on the moon?",
            listOf("1958", "1962", "1967", "1969"),
            3
        )

        val question5 = Question(
            "What is the capital of Romania?",
            listOf("Bucarest", "Warsaw", "Budapest", "Berlin"),
            0
        )

        val question6 = Question(
            "Who did the Mona Lisa paint?",
            listOf("Michelangelo", "Leonardo Da Vinci", "Raphael", "Carravagio"),
            1
        )

        val question7 = Question(
            "In which city is the composer Frédéric Chopin buried?",
            listOf("Strasbourg", "Warsaw", "Paris", "Moscow"),
            2
        )

        val question8 = Question(
            "What is the country top-level domain of Belgium?",
            listOf(".bg", ".bm", ".bl", ".be"),
            3
        )

        val question9 = Question(
            "What is the house number of The Simpsons?",
            listOf("42", "101", "666", "742"),
            3
        )

        return QuestionBank(
            listOf(
                question1,
                question2,
                question3,
                question4,
                question5,
                question6,
                question7,
                question8,
                question9
            )
        )
    }
}
